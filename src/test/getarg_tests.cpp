#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/test/unit_test.hpp>

#include "util.h"

BOOST_AUTO_TEST_SUITE(getarg_tests)

static void
ResetArgs(const std::string& strArg)
{
    std::vector<std::string> vecArg;
    boost::split(vecArg, strArg, boost::is_space(), boost::token_compress_on);

    // Insert dummy executable name:
    vecArg.insert(vecArg.begin(), "testbitcoin");

    // Convert to char*:
    std::vector<const char*> vecChar;
    BOOST_FOREACH(std::string& s, vecArg)
        vecChar.push_back(s.c_str());

    ParseParameters(vecChar.size(), &vecChar[0]);
}

BOOST_AUTO_TEST_CASE(boolarg)
{
    ResetArgs("-FRC");
    BOOST_CHECK(GetBoolArg("-FRC"));
    BOOST_CHECK(GetBoolArg("-FRC", false));
    BOOST_CHECK(GetBoolArg("-FRC", true));

    BOOST_CHECK(!GetBoolArg("-fo"));
    BOOST_CHECK(!GetBoolArg("-fo", false));
    BOOST_CHECK(GetBoolArg("-fo", true));

    BOOST_CHECK(!GetBoolArg("-FRCo"));
    BOOST_CHECK(!GetBoolArg("-FRCo", false));
    BOOST_CHECK(GetBoolArg("-FRCo", true));

    ResetArgs("-FRC=0");
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", false));
    BOOST_CHECK(!GetBoolArg("-FRC", true));

    ResetArgs("-FRC=1");
    BOOST_CHECK(GetBoolArg("-FRC"));
    BOOST_CHECK(GetBoolArg("-FRC", false));
    BOOST_CHECK(GetBoolArg("-FRC", true));

    // New 0.6 feature: auto-map -nosomething to !-something:
    ResetArgs("-noFRC");
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", false));
    BOOST_CHECK(!GetBoolArg("-FRC", true));

    ResetArgs("-noFRC=1");
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", false));
    BOOST_CHECK(!GetBoolArg("-FRC", true));

    ResetArgs("-FRC -noFRC");  // -FRC should win
    BOOST_CHECK(GetBoolArg("-FRC"));
    BOOST_CHECK(GetBoolArg("-FRC", false));
    BOOST_CHECK(GetBoolArg("-FRC", true));

    ResetArgs("-FRC=1 -noFRC=1");  // -FRC should win
    BOOST_CHECK(GetBoolArg("-FRC"));
    BOOST_CHECK(GetBoolArg("-FRC", false));
    BOOST_CHECK(GetBoolArg("-FRC", true));

    ResetArgs("-FRC=0 -noFRC=0");  // -FRC should win
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", false));
    BOOST_CHECK(!GetBoolArg("-FRC", true));

    // New 0.6 feature: treat -- same as -:
    ResetArgs("--FRC=1");
    BOOST_CHECK(GetBoolArg("-FRC"));
    BOOST_CHECK(GetBoolArg("-FRC", false));
    BOOST_CHECK(GetBoolArg("-FRC", true));

    ResetArgs("--noFRC=1");
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", false));
    BOOST_CHECK(!GetBoolArg("-FRC", true));

}

BOOST_AUTO_TEST_CASE(stringarg)
{
    ResetArgs("");
    BOOST_CHECK_EQUAL(GetArg("-FRC", ""), "");
    BOOST_CHECK_EQUAL(GetArg("-FRC", "eleven"), "eleven");

    ResetArgs("-FRC -FRC");
    BOOST_CHECK_EQUAL(GetArg("-FRC", ""), "");
    BOOST_CHECK_EQUAL(GetArg("-FRC", "eleven"), "");

    ResetArgs("-FRC=");
    BOOST_CHECK_EQUAL(GetArg("-FRC", ""), "");
    BOOST_CHECK_EQUAL(GetArg("-FRC", "eleven"), "");

    ResetArgs("-FRC=11");
    BOOST_CHECK_EQUAL(GetArg("-FRC", ""), "11");
    BOOST_CHECK_EQUAL(GetArg("-FRC", "eleven"), "11");

    ResetArgs("-FRC=eleven");
    BOOST_CHECK_EQUAL(GetArg("-FRC", ""), "eleven");
    BOOST_CHECK_EQUAL(GetArg("-FRC", "eleven"), "eleven");

}

BOOST_AUTO_TEST_CASE(intarg)
{
    ResetArgs("");
    BOOST_CHECK_EQUAL(GetArg("-FRC", 11), 11);
    BOOST_CHECK_EQUAL(GetArg("-FRC", 0), 0);

    ResetArgs("-FRC -FRC");
    BOOST_CHECK_EQUAL(GetArg("-FRC", 11), 0);
    BOOST_CHECK_EQUAL(GetArg("-FRC", 11), 0);

    ResetArgs("-FRC=11 -FRC=12");
    BOOST_CHECK_EQUAL(GetArg("-FRC", 0), 11);
    BOOST_CHECK_EQUAL(GetArg("-FRC", 11), 12);

    ResetArgs("-FRC=NaN -FRC=NotANumber");
    BOOST_CHECK_EQUAL(GetArg("-FRC", 1), 0);
    BOOST_CHECK_EQUAL(GetArg("-FRC", 11), 0);
}

BOOST_AUTO_TEST_CASE(doubledash)
{
    ResetArgs("--FRC");
    BOOST_CHECK_EQUAL(GetBoolArg("-FRC"), true);

    ResetArgs("--FRC=verbose --FRC=1");
    BOOST_CHECK_EQUAL(GetArg("-FRC", ""), "verbose");
    BOOST_CHECK_EQUAL(GetArg("-FRC", 0), 1);
}

BOOST_AUTO_TEST_CASE(boolargno)
{
    ResetArgs("-noFRC");
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", true));
    BOOST_CHECK(!GetBoolArg("-FRC", false));

    ResetArgs("-noFRC=1");
    BOOST_CHECK(!GetBoolArg("-FRC"));
    BOOST_CHECK(!GetBoolArg("-FRC", true));
    BOOST_CHECK(!GetBoolArg("-FRC", false));

    ResetArgs("-noFRC=0");
    BOOST_CHECK(GetBoolArg("-FRC"));
    BOOST_CHECK(GetBoolArg("-FRC", true));
    BOOST_CHECK(GetBoolArg("-FRC", false));

    ResetArgs("-FRC --noFRC");
    BOOST_CHECK(GetBoolArg("-FRC"));

    ResetArgs("-noFRC -FRC"); // FRC always wins:
    BOOST_CHECK(GetBoolArg("-FRC"));
}

BOOST_AUTO_TEST_SUITE_END()
